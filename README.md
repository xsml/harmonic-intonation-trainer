H.-M. Lin and C.-M. Lin, "Harmonic Intonation Trainer: An Open Implementation in Pure Data," in Proceedings of the 15th International Conference on New Interfaces for Musical Expression, Baton Rouge, 2015, pp. 38–39.

programming language: Pure Data

version: 0.45.4

[download the paper](https://www.nime.org/proceedings/2015/nime2015_300.pdf)